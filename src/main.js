// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import Home from './components/home.vue';
import Register from './components/register.vue';
//import comp from './components/todo.vue';
import BootstrapVue from 'bootstrap-vue';
import store from './store/store'

Vue.config.productionTip = false;
Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
  template: '<App/>',
  components: { App/*, comp */,Home, Register}
})
