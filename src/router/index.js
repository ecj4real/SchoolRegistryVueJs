import Vue from 'vue'
import Router from 'vue-router'
import Register from '@/components/register'
import Home from '@/components/home'
import ShowStudents from '@/components/showStudents'

Vue.use(Router)


export default new Router({
  routes: [
//    {
//      path: '/',
//      name: 'TodoApp',
//      component: TodoApp
//    },
//    {
//      path: '/todo',
//      name: 'TodoItem',
//      component: TodoItem
//    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path:'/home',
      alias: '/',
      name: 'Home',
      component: Home
    },
    {
      path:'/students',
      name: 'ShowStudents',
      component: ShowStudents
    }
  ]
})
